#! /bin/bash
#Pulls Orgin Git Repo if specified
if [ "$1" != '' ]; then 
  mkdir origin_repo
  dir=$(pwd)
  echo "Cloning $1"
  git clone $1 ./origin_repo
  if [ "$2" != '' ]; then 
    mv origin_repo/$2 $2
  fi
  if [ "$3" != '' ]; then 
    mv origin_repo/$3 $3
  fi
  if [ "$4" != '' ]; then 
    mv origin_repo/$4 $4
  fi
  if [ "$5" != '' ]; then 
    mv origin_repo/$5 $5
  fi
  if [ "$6" != '' ]; then 
    cp origin_repo/$6/ $dir
  fi
fi
