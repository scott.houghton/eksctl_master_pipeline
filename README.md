# AWS EKS Cluster IaC Automation Pipeline
​
This project provides a Gitlab CI pipeline to automate the creation and updating of AWS EKS Clusters.
​
## Instructions
​
### To use this Pipeline: 
​
* Create an upstream pipeline in a project with the files from the example_upstream_pipeline folder in the repo.
   - Create New Project (Name project "eks_cluster_YOURCLUSTERSNAME")
   - Copy files from example_upsteam_pipeline folder
   - Commit & Push files up to git repo
* Modify .gitlab-ci.yml
  - Update K8S_CLUSTER to your cluster name
  - Update All Variables as need (See explanation of each below)
* Create KMS Key per instructions
* Modify "-non-prod1-eks2.yml" with your EKS settings.
   - Update Cluster Name
   - Update AWS Region
   - Update VPC ID & CIDR Range
   - Update Public & Private subnets (Note both are not needed but recommend. Only Public or Private is needed based on your need)
   - Update Nodegroup name to match your EKS cluster name appended with "ng1" and so on for each needed node group
   - Update Node settings as need (i.e. Instance Type, AMIFamily, etc per documentation https://eksctl.io/usage/schema/ )
   - 
* Rename files to match the name of your new EKS cluster you wish to deploy
​
​
## CI/CD Stages
​
### Using Terraform
​
#### Create New KMS Key for Encrypting EKS Worker Nodes Volumes
    This job creates a KMS Key for encrypting the EBS volumes of the EKS worker nodes.
    KMS key will be created with permission needed to encrypted EBS volumes and be managed by admins
​
##### PIPELINE_ACTION == CREATE_KMS
    Set Pipeline action variable to CREATE_KMS
​
### Using EKSCTL
​
#### Create New EKS Cluster
​
##### PIPELINE_ACTION == CREATE_CLUSTER
​
#### Create New EKS Node Group
​
##### PIPELINE_ACTION == CREATE_NODEGROUP
​
#### Delete EKS Node Group
​
##### PIPELINE_ACTION == DELETE_NODEGROUP
​
#### Update EKS Cluster
​
##### PIPELINE_ACTION == UPDATE_CLUSTER
​
#### Delete EKS Cluster
​
##### PIPELINE_ACTION == DELETE_CLUSTER
​
### Using KUBECTL
​
#### Create EKS Namespaces
​
##### NAMESPACE_YAML == "FILENAME"
​
#### Create EKS RBAC Groups
​
##### RBAC_GROUP_YAML == "FILENAME"
​
#### Apply IAM to RBAC Group Role Map
​
##### MAP_ROLES_YAML == "FILENAME"
​
​
## Upstream CI/CD Variables
* **ACTION: ""**
   - Used to set the stage or stages that should be trigger downstream
* **APPLY_IAM_OIDC: "TRUE"**
   - Used to turn on and off IAM OIDC (https://docs.aws.amazon.com/eks/latest/userguide/enable-iam-roles-for-service-accounts.html)
   - "TRUE" or "FALSE" value
* **EKSCTL_YAML: "non-prod1-eks2.yml"**
   - Used to set the filename of the EKSCTL YAML file used (Should match the name of cluster)
* **NAMESPACE_YAML: "non-prod1-eks2_namespaces.yml"**
   - Used to set the filename of the K8s manifest YAML file to create K8s namespaces
* **RBAC_GROUP_YAML: "non-prod1-eks2_eks-groups-cm.yml"**
   - Used to set the filename of the K8s manifest YAML to create K8s RBAC groups and users
* **MAP_ROLES_YAML: "non-prod1-eks2_aws-auth-cm.yml"**
   - Used to set the filename of the K8s config map manifest YAML to map K8s RBAC to AWS IAM roles
* **K8S_YAML: ""**
   - Used to set the filename of the K8s manifest YAML to apply K8s manifests as needed
* **K8S_FOLDER_YAML: ""**
   - Used to set the folder name of the folder containing K8s manifest YAML to mass apply K8s manifests as needed
* **K8S_CLUSTER: "non-prod1-eks2"**
   - Used to set the K8s Cluster name for use with kubectl
* **ENVIRONMENT: "non-prod"**
   - Used to set the environment
* **K8S_REGION: "us-east-1"**
   - Used to set the AWS EKS region
* **ORIGIN_GIT: "https://gitlab+deploy-token-15:5q3R8x8R45yAQqDvj7JS@gitlab.com/iac/eks_cluster_non-prod1-eks2.git"**
   - Used to set the path to the Git repo of the upstream YAML files to be pulled down by the downstream pipeline
   - Be sure to create a GitLab Deploy key with read-only permissions to the repo for use in this variable (https://docs.gitlab.com/ee/user/project/deploy_tokens/)
​
​

## Downstream variables
##### All variables should be set in upstream pipeline and passed to downstream
##### Below if for reference purposes only
* **PIPELINE_ACTION: ""**
   - Used to set the stage or stages that is trigged by upstream pipeline
* **APPLY_IAM_OIDC: "TRUE"**
   - Used to turn on and off IAM OIDC (https://docs.aws.amazon.com/eks/latest/userguide/enable-iam-roles-for-service-accounts.html)
   - "TRUE" or "FALSE" value
* **EKSCTL_YAML: "non-prod1-eks2.yml"**
   - Used to set the filename of the EKSCTL YAML file used (Should match name of cluster)
* **NAMESPACE_YAML: "non-prod1-eks2_namespaces.yml"**
   - Used to set the filename of the K8s manifest YAML file to create K8s namespaces
* **RBAC_GROUP_YAML: "non-prod1-eks2_eks-groups-cm.yml"**
   - Used to set the filename of the K8s manifest YAML to create K8s RBAC groups and users
* **MAP_ROLES_YAML: "non-prod1-eks2_aws-auth-cm.yml"**
   - Used to set the filename of the K8s config map manifest YAML to map K8s RBAC to AWS IAM roles
* **K8S_YAML: ""**
   - Used to set the filename of the K8s manifest YAML to apply K8s manifests as needed
* **K8S_FOLDER_YAML: ""**
   - Used to set the folder name of the folder containing K8s manifest YAML to mass apply K8s manifests as needed
* **K8S_CLUSTER: "non-prod1-eks2"**
   - Used to set the K8s Cluster name for use with kubectl
* **ENVIRONMENT: "non-prod"**
   - Used to set the to set the environment
* **K8S_REGION: "us-east-1"**
   - Used to set the AWS EKS region
* **ORIGIN_GIT: "https://gitlab+deploy-token-15:5q3R8x8R45yAQqDvj7JS@gitlab.com/iac/eks_cluster_non-prod1-eks2.git"**
   - Used to set the path to the Git repo of the upstream YAML files to be pulled down by the downstream pipeline
   - Be sure to create a GitLab Deploy key with read only permissions to the repo for use in this variable (https://docs.gitlab.com/ee/user/project/deploy_tokens/)
​
## GitLab Runner Operation